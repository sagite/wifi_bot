#!/usr/bin/env python

# Learning Algorithm

from robot import InitializeRobot
from predictor import Predictor
from cluster import Cluster
from db import DB
import datetime
import sklearn 

#TODO-all
class LearningAlgorithm:
	'''
	this class implements the learning algorithm
	'''

	def __init__(self,predictor,db):
		self.predictor=predictor
		self.cluster=Cluster()
		self.db=db
		self.run()

	def run(self):

		#flag that holds if the top_k_to_sample has been decided and 
		already_decided_flag=False
		
		while(1):
			#if has not been decided yet and also robot at dock:
			if already_decided_flag==True:
				if robot.in_dock()==True:
					
					#groups=cluster.update_cluster(self.db,m)

					#raffle 60(minutes)*2(hours) points 
					all_points_to_sample=util.points_raffle()

					# ideally 
					#self.top_k_to_sample=predictor.predict(self.db, all_points_to_sample,k)
					self.top_k_to_sample=predictor.predict_k_places(self.db, all_points_to_sample,k)

					self.top_l_to_sample=palnning.plan_route(self.top_k_to_sample)

					already_decided_flag=True

				#if the top_k_to_sample has been decided and also fully charged:
				else:
					if robot.fully_charged==True:
						robot.sample_places_list(self.top_l_to_sample)	
			

			'''
			#TODO-need to handle the robot connection and disconnection
			if rospy.is_shutdown():
				break'''



def main():
	database=DB()
	robot=InitializeRobot(database)
	#predictor=Predictor(robot,database)

	LearningAlgorithm(predictor,database)
	
if __name__ == "__main__":
		main()
					