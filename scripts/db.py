#!/usr/bin/env python
# DB

import pymongo
import bson
import util
from pymongo import MongoClient
import datetime
import os,sys


class Wifistats:
	def __init__(self):
		self.wifistats_t=[]
		import csv
		with open('./../wifistats.csv', 'rb') as csvfile:
			for row in csv.reader(csvfile) :
				self.wifistats_t.append(row)

	def find(self):
		print self.wifistats_t
		return self.wifistats_t

class DB:

	def __init__(self):
		self.client = MongoClient()
		self.db = self.client.local
		self.mornings = []
		self.wifistats_t=[]


		#if mongodb is empty, create a class that is similar to wifistats inside mongoDB:
		if self.db.wifistats is None:
			self.wifistats_table=Wifistats()

		else:	
			for doc in self.db.wifistats.find():
				self.wifistats_t.append(doc)
				#print self.wifistats_t		
			self.wifistats_table = self.db.wifistats
			#print self.db.wifistats_table

	def find_in_db(self,fdate,fday,ftime):
		return db.find({'date':fdate},{'day':fday},{'time':ftime})

	#converting the time field of type string to be double type.
	def convert_Time_To_Double(self):
		for doc in self.wifistats_table.find():
			newDoubleTime = timeConvert(doc['time'])
			self.wifistats_table.update({'_id': doc['_id']},{'$set':{'time': newDoubleTime}})

	#converting the day field of type string into categories(0-Sunday, 1-Monday...)
	def convert_day_to_category_number(self):

		for doc in self.wifistats_table.find():
			newCategoryDay = dayConvert(doc['day'])

			if newCategoryDay is not None:
				self.wifistats_table.update({'_id': doc['_id']},{"$set": {"day": newCategoryDay}});

	def convert_place_to_category_number(self):
		for doc in self.wifistats_table.find():
			newCategoryPlace = placeConvert(doc['place'])

			if newCategoryPlace is not None:
				self.wifistats_table.update({'_id': doc['_id']},{"$set": {"place": newCategoryPlace}});
			else:
				print "err", doc['place']


	def Print_Collection(self):
		count=0
		for doc in self.wifistats_table.find():
			print doc
			count+=1
			print count

	def drop_collection(self):
		self.db.wifistats.drop()

	def new_collection(self,name='wifistats'):
		'''csv file in home directory:'''
		os.system('mongoimport -d local -c wifistats --type csv --file ~/' +name+'.csv --headerline')
	

	"""def find_morning_in_db(self):
		value1 = datetime.datetime.strptime('08:00', '%H:%M').time()
		value2 = datetime.datetime.strptime('12:00', '%H:%M').time()
		self.mornings = self.wifistats_table.find({'time': {'$gt': value1, '$lt': value2}})
		for doc in self.mornings:
			print doc"""

	# converting the time field of type string to be datetime type. --> need to finish
	"""
	def fixTime(self):
		for obj in self.wifistats_table.find():
			if obj['time']:
				if type(obj['time']) is not datetime:
					time = datetime.datetime.strptime(obj['time'],'%H:%M')
					self.wifistats_table.update({'_id':obj['_id']},{'$set':{'time' : time}})
	"""




#running trials
#database = DB()
#database.drop_collection()
#database.new_collection()

#database.create_new_collection("suzi")
#database.convert_day_to_category_number()
#database.convert_Time_To_Double()
#database.Print_Collection()
#database.convert_place_to_category_number()

