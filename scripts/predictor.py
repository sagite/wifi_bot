#!/usr/bin/env python

# Predictor

#import robot
#from db import db
import datetime
import math
import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neighbors import NearestNeighbors
from matplotlib import style
style.use("ggplot") 
import db
import util

OFFSET=30
TIMES=range(0,60*24,OFFSET)
DAYES=('Sun','Mon','Tue','Wed','Thu','Fri','Sat')
WIFI_RANKS=range(1,5)



class Predictor:
	''' 
	this class handles the predictor 
	'''
	def __init__(self,db):
		#self.rob=rob
		self.db=db

	#TODO 
	def predict(self,place,time):
		'''
		this func recieves a place and time and predict according to our favourite predicting method
		returns a tuple of precentage of the quality of (ping,upload,download)
		'''
		pass
		
	#TODO 
	def predict_k_places(self,db,groups,all_points_to_sample,k):
		''' this func recieves tuple of points (location and features of time) to predict, take the most similar data according to the groups and decided for each weather it needs sampling or prediction. 
		there is an oveloading of the function that takes no considiration in the clustering
		for each point predict it and save a tuple, if it important to sample(by distance) then change the tuple only after the sample ocurres.
		time can be a hour, a feature (morning,afternoon,evening etc according to the clustering) so the func should support both options
		'''
		
		today= datetime.datetime.now()
        curr_date = datetime.date.now()
        day = today.strftime('%A')
        self.KNN(all_points_to_sample, k)

        data_predictions = set()
        for dot in all_points_to_sample:
            prediction, sureness = self.predict(dot[0], day, dot[1])
            data_predictions.add((dot, prediction, sureness))     # ((place, time), (predicted_ping, predicted_upload, predicted_download), combined_sureness_value)
            self.db.update_temp_db([dot, curr_date, prediction, 'No']) # need to do this function. function gets dot (place, time), current date, prediction (p_ping, p_upload, p_download), flag 'no'

        most_k_uncertainty = sorted(data_predictions, key = lambda data: data[2])[:self.NUMBER_OF_CHECKS_PER_DAY]

        return most_k_uncertainty
        #return tuple of k most important to sample

		
	# the dataset parameter is the samples(it is of type db.collection), k is the num of neighbors, 
	# we want to predict the [ping,download,upload] that is relevent for the input parameter [day,time] 
	def KNN(self, all_points_to_sample,k):
		
		wifistats = self.db.wifistats_table
		Y = np.zeros((wifistats.count(),3))
		X=np.zeros((wifistats.count(),3))
		temp=all_points_to_sample
		count = 0
		'''go through all labels:'''
		for doc in wifistats.find():
			for feat in xrange(len(util.FEATURES)):
				Y[count][feat] = long(doc[util.FEATURES[feat]])

			X[count][0]=doc['place']
			X[count][1]=doc['day']
			X[count][2]=doc['time']

			count += 1

		
		count = 0
		all_points_to_sample=np.reshape(all_points_to_sample,(len(all_points_to_sample),1))	
		knn = KNeighborsRegressor(algorithm = 'kd_tree',n_neighbors=k)	
		#knn = KNeighborsClassifier( algorithm = 'kd_tree' ,n_neighbors=k)
		
		nbrs = NearestNeighbors(n_neighbors=2, algorithm='ball_tree').fit(X)
		distances, indices = nbrs.kneighbors(X)
		print distances, indices
		print "predict ping, ulpoad, download:"
		ping=knn.fit(tuple(X),Y[:,0])
		print ping.predict(temp)
		upload=knn.fit(tuple(X),Y[:,1])
		print upload.predict(temp)
		download=knn.fit(tuple(X),Y[:,2])
		print download.predict(temp)
		
		linear_mod=linear_model.LinearRegression()
		linear_mod.fit(X,Y[:,0])
		plt.scatter(X,Y,color="yellow")
		plt.plot(X,linear_mod.predict(X),color="blue")
		plt.show()

		
			
	#TODO 
	def get_info_from_db(self,db,location,time,day):
		'''
		this func get info from db according to location, time and day
		'''
		pass

	#TODO
	def rank_place(time,day,location):
		'''
		this func recieves a day time and location and predict the wifi features (ping, upload,download)
		returns tuple of precentage of the wifi features (ping, upload,download)
		'''
		pass



#no need	
	def predictor_accuracy_check(self,time=TIMES,day=DAYES,location=None):
		'''
		this func recieves a location, time and day and check the acurracy of the prediction
		'''
		if location is None:
			location=self.rob.locations

		today=datetime.datetime.now()

		for loc in location:
			self.predict(today.time(), today.strftime('%A'),loc)
		return 70
		#TODO - CV in order() to decide if it's accurate enough

	#no need
	def rank_wifi(self,time=datetime.time(), day=datetime.date.today(),location=None):
		'''
		this func recieves day and time and ranks the wifi for each feature and gives a rank from 1-5
		'''
		if location is None:
			location=rob.locations

		for loc in locations:
			features_predict=self.rank_place(time,day,location)
			# simple ranking
			rank=round(math.avg(features_predict)/20)+1
			return (loc,time,day,features_predict,rank)



def main():
	
	database=db.DB()	
	predictor=Predictor(database)		
	predictor.KNN(((1,2,18.02)),5)


if __name__ == "__main__":
		main()
