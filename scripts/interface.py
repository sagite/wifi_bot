#!/usr/bin/env python

#Interface

import db
from learning_algorithm import LearningAlgorithm
from robot import InitializeRobot
#import rospy
from predictor import Predictor


class Interface:
	'''
	this class initialize the system
	'''
	def __init__(self):
		self.db=db.DB()
		self.robot=InitializeRobot(self.db)
		self.predictor=Predictor(self.robot,self.db)
		
		self.start_running()
		
	def start_running(self):
		'''
		this func starts the entire process of thr learning algo
		'''
		LearningAlgorithm(self.predictor,self.db)
	
def main():
	Interface()
	
if __name__ == "__main__":
		main()
		
			
	