#!/usr/bin/env python

# Cluster

'''Outer libraries import:'''
import datetime
import csv
import time
from pymongo import MongoClient

import sklearn
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from matplotlib import style 
style.use("ggplot")

'''Inner libraries import:'''
import db
import util

class Cluster:
	''' 
	this class handles the clusterer 
	'''
	def __init__(self,db):
		self.db=db


	#TODO
	def update_cluster(self):
		'''
		this func recieves the db  data and gatter in groups
		oveloading- decide the number of groups
		return the number of groups and the clusterd data
		'''

		wifistats = self.db.wifistats_table
		X = np.zeros((wifistats.count(),3))
	
		count = 0
		'''go through all labels:'''
		for doc in wifistats.find().sort([["_id",1]]):
			for feat in len(util.FEATURES):
				X[count][feat] = doc[util.FEATURES[feat]]
				
			count += 1
	

		kmeans = KMeans(n_clusters = 3)
		kmeans.fit(X)
		kmeans.predict(X)
		print "prediction "

		centroids = kmeans.cluster_centers_
		labels = kmeans.labels_
		
		if "cluster1" in self.db.db.collection_names():
			self.db.db.cluster1.drop()
		if "cluster2" in self.db.db.collection_names():
			self.db.db.cluster2.drop()
		if "cluster" in self.db.db.collection_names():
			self.db.db.cluster.drop()
		if "cluster6" in self.db.db.collection_names():
			self.db.db.cluster6.drop()
		
		self.db.db["cluster6"]

		count = 0
		for doc in wifistats.find().sort([["_id",1]]):
			self.db.db.cluster6.insert(doc)
			self.db.db.cluster6.update({"_id": doc["_id"]} ,{"$set": {"label": int(labels[count])}})
			count += 1

		for doc in self.db.db.cluster6.find().sort([["_id",1]]):
			if doc["label"] is not 0:
				self.db.db.cluster6.delete_one(doc)
		for doc in self.db.db.cluster6.find().sort([["_id",1]]):
			print doc


		colors = ["g.","r.","c.","y.","b."]

		for i in range(len(X)):
			print ("coordinate:",X[i], "labels:",labels[i])
			plt.plot(X[i][0],X[i][1], colors[labels[i]], markersize = 10)
		plt.scatter(centroids[:, 0], centroids[:, 1], marker = "x", s = 150, linewidths = 5, zorder = 10)
		plt.show()

	def new_cluster(self):
		X = np.zeros((wifistats.count(),3))
	
		count = 0
		for doc in self.db.db.cluster.find().sort([["_id",1]]):
			X[count][0] = doc['ping']
			X[count][0] = doc['download']
			X[count][1] = doc['upload']
			count += 1

		kmeans = KMeans(n_clusters = 3)
		kmeans.fit(X)

		centroids = kmeans.cluster_centers_
		labels = kmeans.labels_

		colors = ["g.","r.","c.","y.","b."]

		for i in range(len(X)-1):
			print ("coordinate:",X[i], "labels:",labels[i])
			plt.plot(X[i][0],X[i][1], colors[labels[i]], markersize = 10)
		plt.scatter(centroids[:, 0], centroids[:, 1], marker = "x", s = 150, linewidths = 5, zorder = 10)
		plt.show()



def main():
	database=db.DB()
	cluster=Cluster(database)
	#cluster.update_cluster()
	
	print util.points_raffle()
	
if __name__ == "__main__":
		main()
				