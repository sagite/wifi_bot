#!/usr/bin/env python

# utils and definitions

import datetime
from itertools import product



FEATURES=('ping','upload','download')
DAYS=('Sunday', 'Monday','Tuesday','Wedensday', 'Thursday') #xrange(0,6)
PLACES=('nili','ana','back-library','library') #xrange(0,3) 

#converting string time to double number
def timeConvert(trial):
	(hours, minutes, seconds) = [int(x) for x in trial.split(":")]
	long_number = hours + (long(minutes)/60)
	print long_number
	return long_number

def dayConvert(strDay):
	str_day = str(strDay)
	print str_day
	for day in xrange(0,len(DAYS)-1):
		print "DAYS[day]", DAYS[day], "day", day
		if str_day==DAYS[day]:
			return day
	return None

def placeConvert(strPlace):
	str_place = str(strPlace)
	print str_place
	for place in xrange(0,len(PLACES)-1):
		print "PLACES[place]", PLACES[place], "place", place
		if str_place==PLACES[place]:
			return place
	return None

def get_today():
	today=datetime.datetime.now()
	return{'time':today.strftime('%H:%M:%S'),'day':today.strftime('%A')}


def points_raffle():
	''' 
	this func raffle the points to sample/predict
	60*2 points - 2 hours 60 minutes
	return a tuple of tuples of the format: (place, time)
	'''
	today=get_today()
	time=timeConvert(today['time'])
	day=today['day']

	hour=int(time)
	minute=int ((time-hour)*100)
	print minute
	minutes_1=range(0,minute)
	minutes_2=range(minute,60)

	hours=range(hour,hour+2)
	places=PLACES

	temp_times=tuple(product(range(hour,hour+2),minutes_2))+tuple(product(range(hour+1,hour+3),minutes_1))
	times=[]

	for h,m in temp_times:
		times.append(h+float(m)/100)

	#print times
	print product(places,tuple(times))
	return product(places,times)

#no need
def manage_time(time):
	'''
	this func recieves axact time and associate it to half an hour
	'''
	pass


if __name__ == '__main__' :
	x=points_raffle()
	print x